<?php
namespace App\Controllers;

use App\Core\App;

class TasksController
{
    public function index(){
        $tasks = App::resolve('queryBuilder')->selectAll('todos', 'App\\Controllers\\Task');

        return view('tasks', compact('tasks'));
    }

    public function store(){
        App::resolve('queryBuilder')->insert('todos', [
            'description' => $_POST['description']
        ]);

        return redirect('tasks');
    }
}