<?php
use App\Core\App;
use App\Core\Database\Connection;
use App\Core\Database\QueryBuilder;

App::bind('config', require 'config.php');

App::bind('queryBuilder', new QueryBuilder(
    Connection::make(
        App::resolve('config')['database']
    )
));

function view($file, $data = []){
    extract($data);
    return require "app/views/{$file}.view.php";
}

function redirect($path){
    header("Location: /{$path}");
}