<?php

namespace App\Core;

class App
{
    protected static $registry = [];

    public static function bind($key, $value){
        static::$registry[$key] = $value;
    }

    public static function resolve($key){
        if(!array_key_exists($key, static::$registry)){
            throw new Exception("No {$key} bound into container");
        }

        return static::$registry[$key];
    }
}