<?php require 'partials/header.php' ?>

<main>
    <ul>
        <?php foreach ($tasks as $task) : ?>
            <li>
                <?php if($task->isComplete()) : ?>
                    <strike><?= $task->description; ?></strike>
                <?php else : ?>
                    <?= $task->description; ?>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
    </ul>

    <form method="post" action="/tasks">
        <input name="description">
        <input type="submit" value="Submit">
    </form>
</main>
<?php require 'partials/footer.php' ?>