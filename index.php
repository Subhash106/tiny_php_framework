<?php
error_reporting(-1);
ini_set('display_errors', 1);

use App\Core\Router;
use App\Core\Request;

require 'vendor/autoload.php';
require 'core/bootstrap.php';

$router = new Router();

Router::load('app/routes.php')->direct(Request::uri(), Request::method());


/*class Post{
    public $title;
    public $published;
    public $votes;
    public function __construct($title, $published, $votes)
    {
        $this->title = $title;
        $this->published = $published;
        $this->votes = $votes;
    }
}

$posts = [
    new Post('My first post', true, 10),
    new Post('My second post', true, 4),
    new Post('My third post', false, 2),
    new Post('My fourth post', true, 9),
    new Post('My fifth post', false, 6)
];

$publishedPosts = array_filter($posts, function ($post){
    return $post->published;
});

$publishedPosts = array_map(function ($post){
    $post->published = true;

    return $post;
}, $posts);

$titles = array_column($posts, 'title');
$totalVotes = array_reduce($posts, function ($sum, $post){
    $sum += $post->votes;
    return $sum;
}, 10);

var_dump($totalVotes);*/

