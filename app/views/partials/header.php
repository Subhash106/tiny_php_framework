<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Tiny Framework</title>
        <link rel="stylesheet" href="/public/css/style.css">
    </head>
    <body>
        <header>
            <nav>
                <ul>
                    <li><a href="/">Home</a> </li>
                    <li><a href="/about">About</a> </li>
                    <li><a href="/about/culture">About Culture</a> </li>
                    <li><a href="/contact">Contact</a> </li>
                    <li><a href="/tasks">Tasks</a> </li>
                </ul>
            </nav>
            <h1>Tasks for the day</h1>
        </header>